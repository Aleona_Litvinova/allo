﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.POM
{
    class Cart
    {
 private IWebDriver _driver;


            //текст на пустой корзине
            public By mainTextForEmptyCart = By.XPath("/html/body/div[4]/div/div/div[3]/div/div[1]/div/p[1]");
            //"оформить заказ"
            public By clickMakeOrderButton = By.XPath("/html/body/div[4]/div/div/div[3]/div/div[1]/footer/div/button");

            public By getFieldName = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/div/div[1]/div[2]/div/input");
            public By getFieldTelephone = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div/input");
            public By getFieldEmail = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/div/div[3]/div[2]/div/input");
            public By getFieldCity = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/div/div[4]/div[2]/p");
            public By getChangeCity = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/div/div[4]/div[2]/div[2]/div[1]/ul/li[2]");
            public By chooseDeliveryAndPaymantButton = By.XPath("/html/body/div[1]/div/div/div[2]/div/div[1]/div[1]/div/div/div[2]/div/button");



            public Cart(IWebDriver driver)
            {
                this._driver = driver;
            }

            //метод который будет искать надпись на пустой корзине странице
            public IWebElement FindEmptyCartLabel()
            {
                return _driver.FindElement(mainTextForEmptyCart);
            }
            //элемент который достанет текст с mainTextForEmptyCart
            public string GetTextFromEmptyCartLabel()
            {
                return FindEmptyCartLabel().Text;
            }


            //click make order
            public Cart ClickOnMakeOrderButton()
            {
                _driver.FindElement(clickMakeOrderButton).Click();
                return this;
            }








        }
    }



