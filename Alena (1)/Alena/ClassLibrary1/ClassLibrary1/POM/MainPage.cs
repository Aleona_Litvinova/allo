﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.POM
{
    public class MainPage
    {
        private IWebDriver _driver;
        public By LinkOnMainPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a/img[1] ");
        public By Localization = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By whiteTeam = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[2]");
        public By BlackTeam = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[2]/span[1]");
        public By Blog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By Fishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[1]/div");
        public By Vacancie = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By Stores = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By Delivery = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By Credit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a")
        // корзина
        public By getToCart = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[1]");
        //товар                                     
        public By addProduct = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[3]/div[2]/div[1]/div[2]/div[2]/button/svg");

        public MainPage(IWebDriver driver)
        {
            this._driver = driver;
        }


        //клик на "Кoрзину"
        public Cart ClickOnCart()
        {
            _driver.FindElement(getToCart).Click();
            return new Cart(_driver);
        }

        //клик на товаре
        public Cart ClickOnProduct()
        {
            _driver.FindElement(addProduct).Click();
            return new Cart(_driver);
        }




    }
}