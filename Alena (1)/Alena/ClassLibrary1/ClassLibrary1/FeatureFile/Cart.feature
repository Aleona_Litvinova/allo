﻿Feature: Cart
	As a user 
	I want to have acces to my cart
	In order to view a content of my order any time

@mytag
Scenario: Unautorized user has empty cart
	Given Allo website is open
	Given User is not logged in
	When User click on cart
	Then cart is empty

	Scenario: Unautorized user orders two products 
	Given Allo website is open
	Given User is not logged in
	When User clik on buttn buy on the pruduct
	Then One product is in the cart
	When User click  on button plus
	Then Count of product is  two
	When User click  on the buttn make order
	Then Form of order is opend
	When User enter name
	When User enter phone number
	When User enter email
	When User click on buttn choose delivery
	Then Order is made

	Scenario: Unautorized user delete product from the cart
	Given Allo website is open
	Given User is not logged in
	When User click on buttn buy on the pruduct
	When User click on button continue serch product
	When User click on button delete first product
	When User click on button delete second product 
	Then cart is empty
