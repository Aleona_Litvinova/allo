﻿using System;
using TechTalk.SpecFlow;

namespace ClassLibrary1.Steps
{
    [Binding]
    public class CartSteps
    {
        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clik on cart")]
        public void WhenUserClicOnCart()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"cart is empty")]
        public void ThenCfrtIsEmpty()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
